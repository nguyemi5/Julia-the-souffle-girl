/*******************************************************************
  Simple program to check LCD functionality on MicroZed
  based MZ_APO board designed by Petr Porazil at PiKRON

  mzapo_lcdtest.c       - main and only file

  (C) Copyright 2004 - 2017 by Pavel Pisa
      e-mail:   pisa@cmp.felk.cvut.cz
      homepage: http://cmp.felk.cvut.cz/~pisa
      work:     http://www.pikron.com/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include <complex.h>
#include <string.h>
#include <pthread.h>

#include <errno.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

void print_to_lcd(unsigned char* mem_base, uint16_t* disp, int height, int width)
void Julia(uint16_t* win, int h, int w, int s1, int s2, float _Complex c, int prec, float o1, float o2, float zoom, float kr, float kg, float kb)
void writetext(uint16_t* disp, char* str, int startx, int starty, uint16_t colort, uint16_t colorb, _Bool reverse)

void* display(void*);
void* UDP(void*);

///Julia variables
float _Complex c = -0.4+0.6*I;
int precision = 50;	
	
char str[100];
int len;
int piece;
int s1=160; //display value of 0+0i
int s2=240;
int size=0;
float o1=0; //center coords - coords of display center
float o2=0;
float zoom = 150.0;

///panorama variables
int lenpanorama = 9;
float _Complex* panorama;
int a; // panorama[a]
int b; // increment a slowly

_Bool ret = false;
_Bool menu = true;

///display + text variables
int letheight = 16;
int letwidth = 8;
uint16_t color1 = 0xffff;
uint16_t color2 = 0x0000;
	
int height = 320;
int width = 480;
uint16_t *disp;
unsigned char *parlcd_mem_base;
unsigned char *mem_base;

int state=0; //0=classic, 1=zoom+parameter, 2=panorama, 3=color brightness, 4=set to default, 5=quit

///menu variables
char* list[7];
uint8_t option = 0;
int lenmenu = 6;

///thread sync
pthread_mutex_t mtx; //mtx - menu option
pthread_mutex_t mtxc; //mtx - parameters change

///color brightness
float kr=1;
float kg=1;
float kb=1;

int main(int argc, char *argv[])
{	
	parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

	if (mem_base == NULL)
		exit(1);
	
	disp = malloc(width*height*sizeof(uint16_t));
	if(disp==NULL)
	{
		printf("ERROR: Malloc failed\n");
		exit(1);
	}

	uint8_t rgb_knobs_value;
	uint8_t rgb_knobs_value2;
	uint8_t rgb_knobs_value3;
	
	uint8_t knb1;
	uint8_t knb2;
	uint8_t knb3;
	
	int dif1=0;
	int dif2=0;
	int dif3=0;
	
	
	knb1 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
	knb2 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 1);
	knb3 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 2);

	size = height*width;
	state = 1;
	
	///prepare for menu
	list[0] = "1. Move around + iterations";
	list[1] = "2. Zoom + change parameter c";
	list[2] = "3. Panorama";
	list[3] = "4. Color brightness";
	list[4] = "5. Set parameters to default";
	list[5] = "6. Quit";
	list[6] = "Press red button to confirm";
	
	///prepare for panorama
	panorama = malloc(lenpanorama*sizeof(float _Complex));
	panorama[0] = -0.4+0.6*I;
	panorama[1] = 0.285;
	panorama[2] = 0.285 + 0.1*I;
	panorama[3] = 0.45 + 0.1428*I;
	panorama[4] = -0.70176 - 0.3842*I;
	panorama[5] = -0.835-0.2321*I;
	panorama[6] = -0.8 + 0.156*I;
	panorama[7] = -0.7269 + 0.1889*I;
	panorama[8] = -0.8*I;
	
	pthread_t thrs[2];
	pthread_create(&thrs[0], NULL, display, NULL);
	printf("Thread display created\n");
	pthread_create(&thrs[1], NULL, UDP, NULL);
	printf("Thread UDP created\n");
	pthread_mutex_init(&mtx, NULL);
	pthread_mutex_init(&mtxc, NULL);
	
	while(!ret)
	{	
	rgb_knobs_value = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o); //blue knob
	rgb_knobs_value2 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 1); //green knob
	rgb_knobs_value3 = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 2); //red knob
	

	dif1=rgb_knobs_value - knb1;
	if(dif1>128)
	{
		dif1-=255;
	}
	else if(dif1<-128)
		dif1+=255;
		
	dif2 = rgb_knobs_value2-knb2;
	if(dif2>128) 
	{
		dif2-=255;
	}
	else if(dif2<-128)
		dif2+=255;
	
	dif3 = rgb_knobs_value3-knb3;
	if(dif3>128) 
	{
		dif3-=255;
	}
	else if(dif3<-128)
		dif3+=255;
	
	if(menu)
	{
		pthread_mutex_lock(&mtx); //mutex to avoid color changes in menu
			option = (option+dif1)%lenmenu;		
		pthread_mutex_unlock(&mtx);
	}
	else if(state==0) //classic - Julia, move around + precision
	{
		pthread_mutex_lock(&mtxc);
			s2+=dif2;
			s1+=dif1;
			precision+=dif3;
		pthread_mutex_unlock(&mtxc);
	}
	else if(state==1) //zoom + increment c
	{
		pthread_mutex_lock(&mtxc);
			zoom = zoom + dif3;
			c = c + dif2/1000.0f + dif1/1000.0f*I;	
		pthread_mutex_unlock(&mtxc);
	}
	
	else if(state==2) //panorama
	{
		b=(b+1)%9000000;
		a = (b/1000000);//%lenpanorama;
		
		pthread_mutex_lock(&mtxc);
			c = panorama[a];
		pthread_mutex_unlock(&mtxc);			
	}
	
	else if(state==3) //color brightness
	{
		pthread_mutex_lock(&mtxc);
			kr += dif3/100.0f;
			kg += dif2/100.0f;
			kb += dif1/100.0f;
			sprintf(str, "r: %2.0f, g: %2.0f, b:%2.0f [MENU]", kr*100, kg*100, kb*100);
		pthread_mutex_unlock(&mtxc);
	}		
	
	else if(state==4) //parameters set to default
	{
		pthread_mutex_lock(&mtxc);
			c = -0.4 + 0.6*I;
			precision = 50;
			zoom = 150;
			s1 = 0;
			s2 = 0;
		pthread_mutex_unlock(&mtxc);
		state = 0; //go back to state classic 
	}
	
	knb1 = rgb_knobs_value;
	knb2 = rgb_knobs_value2;
	knb3 = rgb_knobs_value3;
	
	if(((*(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 3)<<4)>>4)==1) //push blue - go to menu
		menu = true;
	else if (((*(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 3)<<2)>>4)==1) //push red - return from menu
	{
		if(menu)
		{
			state = option;
			menu = false;
		}
	}
	if(state==5)
		ret = true;
	
	} //while(!ret)
	
  pthread_join(thrs[0], NULL); 
  pthread_join(thrs[1], NULL);

  return 0;
}

void* display(void*v)
{
	
	uint16_t colortemp;
	while(!ret)
	{
		if(menu)
		{
			pthread_mutex_lock(&mtx);
			
			for(int l=0; l<size; l++) //white screen
			{
				disp[l] = 0xffff;
			}
			
			for(int m=0; m<(lenmenu+1); m++)
			{
				sprintf(str, "%s", list[m]);
				if(m==option)
				    writetext(disp, str, 0, 0, color1, color2, false); //invert colors to higlight choice
				else
				    writetext(disp, str, 0, 0, color2, color1, false);
			}
			
			pthread_mutex_unlock(&mtx);
			
		}		
		else
		{	
			pthread_mutex_lock(&mtxc);
			Julia(disp, height, width, s1, s2, c, precision, o1, o2, zoom, kr, kg, kb);
			
			if(state!=3) //not changing color brightness
			{
				if(cimagf(c)>=0)
				sprintf(str, "O:[%1.2f, %1.2f i],c=%1.2f+%1.2fi,iter=%i,zoom=%4.0f [MENU]", o1, o2, crealf(c), cimagf(c), precision, zoom);
				else
				sprintf(str, "O:[%1.2f, %1.2f i],c=%1.2f%1.2fi,iter=%i,zoom=%4.0f [MENU]", o1, o2, crealf(c), cimagf(c), precision, zoom);
			}
			writetext(disp, str, 0, 0, color2, color1, true);
			pthread_mutex_unlock(&mtxc);
		}
			
		print_to_lcd(parlcd_mem_base, disp, height, width);	
	
	} //end while(!ret)
	
	return NULL;
}

void* UDP(void* v)
{
   int sockfd;
   char udp_buffer[50];
   int succ;
   char m;
   float a;
   float b;
 
	//initialization of socket
   if ((sockfd = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0)) == -1) {
		perror("socket");
		exit(1);
	}
	
	struct sockaddr_in bindaddr;

	memset(&bindaddr, 0, sizeof(bindaddr));
	bindaddr.sin_family = AF_INET;
	bindaddr.sin_port = htons(44444);
	bindaddr.sin_addr.s_addr = INADDR_ANY;

	if (bind(sockfd, (struct sockaddr *)&bindaddr, sizeof(bindaddr)) == -1) {
	perror("bind");
	exit(1);
	}

	int yes=1;

	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
				sizeof(yes)) == -1) {
	perror("setsockopt (SO_REUSEADDR)");
	exit(1);
	}
	
	while(!ret)
	{
		succ = recv(sockfd, udp_buffer, sizeof(udp_buffer), 0);
		if(succ>0)
		{
			succ = sscanf(udp_buffer, "%c %f %f", &m, &a, &b);
		}
		if(succ>1)
		{
			pthread_mutex_lock(&mtxc);
			switch(m)
			{
				case 'n':
						precision = a;
						printf("Number of iterations changed to n = %i\n", precision);
					break;
				case 'c':
					if(succ<3)
					{
							c = a + cimagf(c)*I;
							printf("Parameter c changed to: c = %3.3f + %3.3f\n", crealf(c), cimagf(c));
					}
					else
					{
							c = a + b*I;
							printf("Parameter c changed to: c = %3.3f + %3.3f\n", crealf(c), cimagf(c));
					}
					break;
				case 'z':
						zoom = a;
						printf("Zoom changed to: z = %4.0f\n", zoom);
					break;
				default:
					printf("Character not recognized\n");
					break;
			}
			pthread_mutex_unlock(&mtxc);
		}
	}
	return NULL;
}

void print_to_lcd(unsigned char* mem_base, uint16_t* disp, int height, int width)
{
	uint16_t c;
	parlcd_write_cmd(mem_base, 0x2c);
	int size = width*height;
	for(int i=0; i<size; i++)
	{
		c = disp[i];
		parlcd_write_data(mem_base, c);
	}
}

void Julia(uint16_t* win, int h, int w, int s1, int s2, float _Complex c, int prec, float o1, float o2, float zoom, float kr, float kg, float kb)
{
	float _Complex z0;
	float k;
	float t;
	float r;
	float g;
	float b;
	
	o1 = (s1-160)/zoom;
	o2 = (s2-240)/zoom;

	for(int i=0; i<h; i++)
	{
		
		for(int j=0; j<w; j++)
		{
			z0 = (j-s2)/zoom + ((s1-i)/zoom)*I;
			k=0;
			for(int m=0; m<prec; m++)
			{
				if((crealf(z0)*crealf(z0) + cimagf(z0)*cimagf(z0))>=4)
					break;
				z0 = z0*z0 + c;
				k+=1;
			}
			t = k/prec;
			r = 9*(1-t)*t*t*t*31*kr;
			if(r>31)
				r=31;
			else if(r<0)
				r=0;
			g = 15*(1-t)*(1-t)*t*t*63*kg;
			if(g>63)
				g=63;
			else if(g<0)
				g=0;
			b = 8.5*(1-t)*(1-t)*(1-t)*t*31*kb;
			if(b>31)
				b=31;
			else if(b<0)
				b=0;
			
			win[i*w+j] = r;
			win[i*w+j] = (win[i*w+j]<<6) + g;
			win[i*w+j] = (win[i*w+j]<<5) + b;
		}
	}
	return;
}

void writetext(uint16_t* disp, char* str, int startx, int starty, uint16_t colort, uint16_t colorb, _Bool reverse)
{
    len = strlen(str);
    if(reverse)
    {
		for(int i=0; i<letheight; i++) //write letters
		{
			for(int j=0; j<len; j++)
			{
				piece = font_rom8x16.bits[((str[j]<<4)+i)]>>8;
				for(int k=0; k<letwidth; k++)
				{
					if(piece%2 == 1)
					{
						disp[size-(starty+letheight)*width + i*width - len*letwidth + (j+1)*letwidth - 1 - k - startx] = colort;
					}
					else
					{
						disp[size-(starty+letheight)*width + i*width - len*letwidth + (j+1)*letwidth - 1 - k - startx] = colorb;
					}
					piece /= 2;
				}
			}
		}   
    }
    else
    {
    	for(int i=0; i<letheight; i++) //write letters
		{
			for(int j=0; j<strlen(str); j++)
			{
				piece = font_rom8x16.bits[((str[j]<<4)+i)]>>8;
				for(int k=0; k<letwidth; k++)
				{
					if(piece%2 == 1)
					{
						disp[(2*starty+1)*letheight*width + i*width + (j+1)*letwidth - 1 - k - startx] = color2;
					}
					else
					{
						disp[(2*starty+1)*letheight*width + i*width + (j+1)*letwidth - 1 - k - startx] = color1;
					}
					piece /= 2;
				}
			}
		}
    }
}
